#Rise Up REST Application
For this exercise, you will have to create a small application displaying a list of users. 
The app is in two parts, front-end and back-end.

##Front-End
The Front-end displays a list of users and a detail page for each of them. 
It is possible to make CRUD actions on them: create, update, delete. 
A beautiful user interface would be appreciated. The front-end must be developed on Codeigniter MVC Framework. It uses a REST Client (Curl, Guzzle�) to call the back-end API.
In this Front / Back model, there is no model on the front-end side, Controller call the back-end directly.

##Back-End
The back-end manages the database and shows REST API. API use an ORM or not to interact with the database. In Rise Up, we use NotORM.
You will have to create a User Resource to GET / POST / PUT / DELETE a user on the associated table.
On each action of the user, the front-end will communicate with the back-end in a Restful way.

##Must-use / Used technologies
- PHP 7 / PHP 7.2.7-0ubuntu0.18.04.2 (cli)
- CodeIgniter (Front-End) /  CodeIgniter 3.1.9
- Slim Framexwork (Back-End) /  Slim Framework 3.10.0
- Database of your choice /  MySQL Ver 14.14 Distrib 5.7.23, for Linux (x86_64)

##INSTALL

###1. Create a databse in MYSQL
```
$ mysql> CREATE DATABASE restapp;
```

###2. Create a database user with and grant accesss to the database restapp
```
$ mysql> CREATE USER restapp_user IDENTIFIED BY 'Restapp_User_Pass_7!';
$ mysql> GRANT ALL PRIVILEGES ON `restapp`.* TO `restapp_user`@`localhost` IDENTIFIED BY 'Restapp_User_Pass_7!';
$ mysql> FLUSH PRIVILEGES;
```

###3. Run the initial script in slimapi database folder
```
$ cd slimapi/database && mysql -u restapp_user -p restapp < restapp.sql
```

###4. Setup virtual hosts for both applications
- slimappi (Back-End)
- restapp (Front-End)

###5. How to Install Slim (only if there's no vendor folder)
```
$ cd slimapi && composer require slim/slim "^3.0"
```