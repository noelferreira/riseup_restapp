-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: restapp
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `notes` text,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'noelferreira@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Noel','Ferreira','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(2,'michaeljackson@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Michael','Jackson','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(3,'peterjackson@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Peter','Jackson','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(4,'davidguilmour@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','David','Guilmour','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(5,'rogerwaters@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Roger','Waters','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(6,'carlossantana@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Carlos','Santana','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(7,'carlsagan@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Carl','Sagan','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(8,'bryanadams@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Bryan','Adams','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(9,'mariecurie@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Marie','Curie','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(10,'nikolatesla@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Nikola','Tesla','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(11,'pepeguardiola@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Pepe','Guardiola','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(12,'josemourinho@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Jose','Mourinho','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(13,'arsenewenger@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Arsène','Wenger','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(14,'lionelmessi@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Lionel','Messi','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(15,'cristianoronaldo@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Cristiano','Ronaldo','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1),(16,'kylianmbappe@noexists.eu','7c4a8d09ca3762af61e59520943dc26494f8941b','Kylian','Mbappé','Lorem ipsum dolor sit amet, autem persius vituperatoribus cum in. Eu doming efficiantur nam. Possit eruditi quo cu, ut justo epicuri hendrerit pri, an sea homero aliquip. Est no atqui quaeque adipisci. In nec doming quodsi labitur.',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-06  8:29:08
