<?php
return [
    'settings' => [
        'debug' => true, // set debug mode
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // Configuración de mi APP
        'app_token_name'   => 'MYAPP-TOKEN',
        'connectionString' => [
            'dns'  => 'mysql:host=localhost;dbname=restapp;charset=utf8',
            'user' => 'restapp_user',
            'pass' => 'Restapp_User_Pass_7!'
        ]

    ],
];
