<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    // Validation errors
    // private $validation = array('message' => '', 'type' => '');
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/users
	 *	- or -
	 * 		http://example.com/index.php/users/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/users/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->template->load('template','users');
	}


    /**
     * Get the list of users
     *
     * @param   void
     * @return  array
     */
	public function list_users()
    {
        // List all the users
        $users = $this->get_all_users();
        
        // Pass validation data to the view
        // $this->data['validation'] = $this->validation;

        // Pass users data to the view
        $this->data['users'] = $users;
        
        // Load template with data
        $this->template->load('template','users_list',$this->data);
    }


    /**
     * Add a new user
     *
     * @param   void
     * @return  array
     */
    public function add()
    {
        $errors = [];

        $data = [
            'password' => $this->input->post('password'),
            'email'   => $this->input->post('email'),
            'first_name'  => $this->input->post('first_name'),
            'last_name'  => $this->input->post('last_name'),
            'notes' =>  $this->input->post('notes')
        ];

        // Check for password matching
        $password_check = $this->input->post('password_check');

        if($password_check != $this->input->post('password'))
        {
            $message = sprintf("<strong>Error: </strong> %s", 'The confirmation password doesn\'t match the entered password.');
            $this->session->set_flashdata('validation_message', $message);
            $this->session->set_flashdata('validation_type', 'alert-danger');

            redirect('Users/list_users');
        }
        
        try
        {
            RestApi::call(RestApiMethod::POST,"users/insert",$data);
        }
        catch(Exception $e)
        {
            if($e->getMessage() === RestApiErrorCode::UNPROCESSABLE_ENTITY)
            {
                $errors = RestApi::getEntityValidationFieldsError();
            }
        }

        if(count($errors) !== 0)
        {
            $message = sprintf("<strong>Error: </strong> %s", $errors);
            $this->session->set_flashdata('validation_message', $message);
            $this->session->set_flashdata('validation_type', 'alert-error');
        }
        else {
            $message = sprintf("<strong>Success: </strong> %s", 'Data was saved!');
            $this->session->set_flashdata('validation_message', $message);
            $this->session->set_flashdata('validation_type', 'alert-success');
        }
        
        redirect('Users/list_users');
    }
    

    /**
     * View a specific user
     *
     * @param   void
     * @return  array
     */
	public function view($id)
    {
        // List all the users
        $user = RestApi::call(RestApiMethod::GET,"users/get/{$id}");

        $this->data['user'] = $user;

        // Load template with data
        $this->template->load('template','view_user',$this->data);        
    }

    /**
     * Edit a specific user
     *
     * @param   void
     * @return  array
     */
	public function edit($id)
    {
        // List all the users
        $user = RestApi::call(RestApiMethod::GET,"users/get/{$id}");

        $this->data['user'] = $user;

        if(!empty($this->input->post()))
        {
            $errors = [];

            $data = [
                'id' => $this->input->post('id'),
                'password' => $this->input->post('password'),
                'email'   => $this->input->post('email'),
                'first_name'  => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'notes' =>  $this->input->post('notes')
                     ];

            // Check for password matching
            $password_check = $this->input->post('password_check');

            if($password_check != $this->input->post('password'))
            {
                $message = sprintf("<strong>Error: </strong> %s", 'The confirmation password doesn\'t match the entered password.');
                $this->session->set_flashdata('validation_message', $message);
                $this->session->set_flashdata('validation_type', 'alert-danger');

                redirect('Users/list_users');
            }
        
            try
            {
                RestApi::call(RestApiMethod::PUT,"users/update/$id",$data);
            }
            catch(Exception $e)
            {
                if($e->getMessage() === RestApiErrorCode::UNPROCESSABLE_ENTITY)
                {
                    $errors = RestApi::getEntityValidationFieldsError();
                }
            }

            if(count($errors) !== 0)
            {
                $message = sprintf("<strong>Error: </strong> %s", $errors);
                $this->session->set_flashdata('validation_message', $message);
                $this->session->set_flashdata('validation_type', 'alert-error');
            }
            else {
                $message = sprintf("<strong>Success: </strong> %s", 'Data was saved!');
                $this->session->set_flashdata('validation_message', $message);
                $this->session->set_flashdata('validation_type', 'alert-success');
            }
        
            redirect('Users/list_users');
        }
        
        // Load template with data
        $this->template->load('template','edit_user',$this->data);        
    }

    
    /**
     * Get the list of users
     * This should be in a model!!!!
     *
     * @return  array
     */
    static function get_all_users()
    {
        return RestApi::call(RestApiMethod::GET,"users/list_all");
    }


    /**
     * Get a specific user
     *
     * @param   $id
     * @return  array
     */
	static function get($id)
    {
        return RestApi::call(RestApiMethod::GET,"users/get/{$id}");
    }


    /**
     * Delete specific user
     *
     * @param   $id
     * @return  array
     */
    static function delete($id)
    {
        try
        {
            RestApi::call(RestApiMethod::DELETE,"users/delete/$id");
        }
        catch(Exception $e)
        {
            if($e->getMessage() === RestApiErrorCode::UNPROCESSABLE_ENTITY)
            {
                $errors = RestApi::getEntityValidationFieldsError();
            }
        }

        if(count($errors) !== 0)
        {
            $message = sprintf("<strong>Error: </strong> %s", $errors);
            $this->session->set_flashdata('validation_message', $message);
            $this->session->set_flashdata('validation_type', 'alert-error');
        }
        else {
            $message = sprintf("<strong>Success: </strong> %s", 'Data was saved!');
            $this->session->set_flashdata('validation_message', $message);
            $this->session->set_flashdata('validation_type', 'alert-success');
        }
        
        redirect('Users/list_users');
    }

}
