<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Rise Up REST Application</title>

        <!--Start Header Links -->
        <?php $this->load->view('page_header_links'); ?>
        <!--Start Header Links-->
        
    </head>

    <body data-spy="scroll" data-target="#header">

        <!--Start Headerr Section-->
        <?php $this->load->view('page_header'); ?>
        <!--End of Header Section-->

        <!--Start of content -->
        <div class="main-content">
            <?= $main_content ?>
        </div>
        <!-- Start of content-->

        <!--Start of footer-->
        <?php $this->load->view('footer'); ?>
        <!--End of footer-->

    </body>

</html>
