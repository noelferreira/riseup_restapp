<section id="footer">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6">
                <div class="copyright">
                    <p>@ 2018 - Rise Up <span><a href="https://riseup.ai/">&#9798;</a></span></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="designer">
                    <p>By <a href="#">nferreira</a></p>
                </div>
            </div>
        </div>
        <!--End of row-->
    </div>
    <!--End of container-->
</section>

<!--Scroll to top-->
<a href="#" id="back-to-top" title="Back to top">&uarr;</a>
<!--End of Scroll to top-->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>-->
<script src="<?php echo base_url('js/jquery-1.12.3.min.js')?>"></script>

<!--Back To Top-->
<script src="<?php echo base_url('js/backtotop.js')?>"></script>

<!--JQuery Click to Scroll down with Menu-->
<script src="<?php echo base_url('js/jquery.localScroll.min.js')?>"></script>
<script src="<?php echo base_url('js/jquery.scrollTo.min.js')?>"></script>

<!--WOW With Animation-->
<script src="<?php echo base_url('js/wow.min.js')?>"></script>
<!--WOW Activated-->
<script>
 new WOW().init();
</script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url('js/bootstrap.min.js')?>"></script>

<!-- Custom JavaScript-->
<script src="<?php echo base_url('js/main.js')?>"></script>
