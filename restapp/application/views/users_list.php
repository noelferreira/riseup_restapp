<div class="container">
    
    <div class="row">

        <div class="page-title">

            <h1>List of Users</h1>

            <div class="container">

                <!-- Messages : start -->
                <?php if(!empty($this->session->flashdata('validation_message'))) : ?>
                    <div class="alert <?=$this->session->flashdata('validation_type')?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->flashdata('validation_message'); ?>
                    </div>
                <?php endif; ?>
                <!-- Messages : end -->
                    
                <div class="row">

                    <div class="col-md-12 col-xs-12">

                        <!-- Add new user button -->
                        <button class="pull-right btn btn-primary btn-md " data-title="Add" data-toggle="modal" data-target="#add" ><i class="fa fa-plus"></i> New user</button>
                    </div>

                </div>
                        
                <div class="row">

                    <div class="col-md-12">

                        <div class="table-responsive">

                            <table id="mytable" class="table table-bordred table-striped">

                                <thead>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Notes</th>
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </thead>

                                <tbody>
                                    <?php foreach($users as $user) : ?>
                                        <tr>
                                            <td><?=$user->id?></td>
                                            <td><?=$user->first_name?></td>
                                            <td><?=$user->last_name?></td>
                                            <td><?=$user->email?></td>
                                            <td><?=$user->notes?></td>
                                            <td><a href="<?php echo site_url('Users/view/'.$user->id)?>" role="button" class="btn btn-primary btn-xs" ><span class="fa fa-eye"></span></a></td>
                                            <td><a href="<?php echo site_url('Users/edit/'.$user->id)?>" role="button" class="btn btn-warning btn-xs" ><span class="fa fa-pencil"></span></a></td>
                                            <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-id="<?$user->id?>"><i class="fa fa-trash"></i></button></p></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                
                            </table>

                        </div>

                    </div>
                </div>
            </div>
            
            <!-- Modal Add User: Start -->
            <?php $this->load->view('add_user')?>
            <!-- Modal Add User: End -->

            <!-- Modal View User: Start -->
            <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="view" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">View Your Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input class="form-control " type="text" placeholder="Mohsin">
                            </div>
                            <div class="form-group">

                                <input class="form-control " type="text" placeholder="Irshad">
                            </div>
                            <div class="form-group">
                                <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- Modal View User: End -->
            
            <!-- Modal Edit User: Start -->
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input class="form-control " type="text" placeholder="Mohsin">
                            </div>
                            <div class="form-group">

                                <input class="form-control " type="text" placeholder="Irshad">
                            </div>
                            <div class="form-group">
                                <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


                            </div>
                        </div>
                        <div class="modal-footer ">
                            <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- Modal Edit User: End -->


            <!-- Modal Delete user: Start -->
            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                        </div>
                        <div class="modal-body">

                            <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this user?</div>

                        </div>
                        <div class="modal-footer ">
                            <a href="<?php echo site_url('Users/delete/'.$user->id)?>" role="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</a>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- Modal Delete user: Start -->

            
        </div>
    </div>
</div>
