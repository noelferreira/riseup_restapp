<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="section_header" style="padding: 80px 0;">
                
                <h2 class="text-center">Update user information</h2>

            </div>

        </div>

    </div>
    
    <div class="col-md-8 well">

        <?php echo form_open('Users/edit/'.$user->id, "class='form-horizontal'"); ?>

        <?php echo form_hidden('id', $user->id); ?>
        
        <div class="row">

            <div class="col-md-12">

                <div class="form-group">

                    <label class="col-sm-3 control-label no-padding-right" for="form-field-first-name">First Name </label>
                    
                    <div class="col-sm-4">
                        <input type="text"  class="form-control" id="form-field-first-name" placeholder="User name" name="first_name"  value="<?=$user->first_name ?>" required>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                
                <div class="form-group">

                    <label class="col-sm-3 control-label no-padding-right" for="form-field-last-name">Last Name </label>
                    
                    <div class="col-sm-4">
                        <input type="text"  class="form-control" id="form-field-last-name" placeholder="User name" name="last_name" value="<?=$user->last_name ?>" required>
                    </div>
                </div>
                
            </div>

        </div>
        
        <div class="row">

            <div class="col-md-12">
                
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right" for="form-field-email">Email address</label>
                    
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="form-field-email" aria-describedby="emailHelp" name="email" placeholder="Enter email" value="<?=$user->email?>" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>

            </div>

        </div>
        
        <div class="row">

            <div class="col-md-12">
                
                <div class="form-group">

                    <label class="col-sm-3 control-label no-padding-right" for="form-field-password">Password</label>

                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="form-field-password"  name="password" placeholder="Enter your password" value="<?php echo set_value('password'); ?>" required>
                    </div>

                </div>

            </div>

        </div>
        
        <div class="row">

            <div class="col-md-12">
                
                <div class="form-group">

                    <label class="col-xs-12 col-md-3 col-sm-3 control-label no-padding-right" for="form-field-password_check">Password check</label>
                    
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <input type="text" class="form-control" id="form-field-password_check"  name="password_check" placeholder="Enter your password again" value="<?php echo set_value('password_check'); ?>" required>
                    </div>

                </div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="form-group">

                    <label class="col-sm-3 control-label no-padding-right" for="form-field-notes">Notes</label>

                    <div class="col-sm-8">
                        <textarea class="form-control" rows="5" id="form-field-notes" name="notes"><?=$user->notes?></textarea>
                    </div>
                    
                </div> 

            </div>
            
        </div>

        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-pencil"></span> Update user</button>

    </div>

    <div class="col-md-4">

        
        <div class="row">
            
            <div class="col-md-12">

                <button data-tiele="Delete user" data-toggle="modal" data-target="#delete" type="button" class="btn btn-danger btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-trash"></span> Delete user</button>

            </div>
            
        </div>

        <hr />

        <a href="<?php echo site_url('Users/list_users')?>" role="button" class="btn btn-default btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Back</a>

    </div>

    
    <?php form_close(); ?>

</div>

<!-- Modal Delete user: Start -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this user?</div>

            </div>
            <div class="modal-footer ">
                <a href="<?php echo site_url('Users/delete/'.$user->id)?>" role="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Modal Delete user: Start -->




