<section id="restapp">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_header text-center">
                    <h2>Rise Up REST Application</h2>
                    <p>For this exercise, you will have to create a small application displaying a list of users.</p>
                    <p>The app is in two parts, front-end and back-end.</p>
                </div>
            </div>
        </div>
        <!--End of row-->
        <div class="row">
            <div class="col-md-4">
                <div class="restapp_news">
                    <div class="single_restapp_item">
                        <div class="restapp_img">
                            <img src="img/front-end.png" alt="">
                        </div>
                        <div class="restapp_content">
                            <a href=""><h3>Front-End</h3></a>
                            <div class="expert">
                                <div class="left-side text-left">
                                    <p class="left_side">
                                        <span class="clock"><i class="fa fa-clock-o"></i></span>
                                        <span class="time">Aug 03, 2018</span>
                                        <a href=""><span class="admin"><i class="fa fa-user"></i> Noel Ferreira</span></a>
                                    </p>
                                </div>
                            </div>
                            <p class="restapp_news_content">The Front-end displays a list of users and a detail page for each of them. It is possible to make CRUD actions on them: create, update, delete. A beautiful user interface would be appreciated ... </p>
                            <a href="" class="restapp_link">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-4-->
            <div class="col-md-4">
                <div class="restapp_news">
                    <div class="single_restapp_item">
                        <div class="restapp_img">
                            <img src="img/back-end.jpg" alt="">
                        </div>
                        <div class="restapp_content">
                            <a href=""><h3>Back-end</h3></a>
                            <div class="expert">
                                <div class="left-side text-left">
                                    <p class="left_side">
                                        <span class="clock"><i class="fa fa-clock-o"></i></span>
                                        <span class="time">Aug 03, 2018</span>
                                        <a href=""><span class="admin"><i class="fa fa-user"></i> Noel Ferreira</span></a>
                                    </p>
                                </div>
                            </div>

                            <p class="restapp_news_content">The back-end manages the database and shows REST API. API use an ORM or not to interact with the database. In Rise Up, we use NotORM ...</p>
                            <a href="" class="restapp_link">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-4-->
            <div class="col-md-4">
                <div class="restapp_news">
                    <div class="single_restapp_item">
                        <div class="restapp_img">
                            <img src="img/technologies.jpg" alt="">
                        </div>
                        <div class="restapp_content">
                            <a href=""><h3>Must-use technologies</h3></a>
                            <div class="expert">
                                <div class="left-side text-left">
                                    <p class="left_side">
                                        <span class="clock"><i class="fa fa-clock-o"></i></span>
                                        <span class="time">Aug 03, 2018</span>
                                        <a href=""><span class="admin"><i class="fa fa-user"></i> Noel Ferreira</span></a>
                                    </p>
                                </div>
                            </div>
                            
                            <p class="restapp_news_content">
                                - PHP 7.2.7-0ubuntu0.18.04.2 (cli) <br />
                                - CodeIgniter 3.1.9 <small>Front-end</small><br />
                                - Slim Framework 3.10.0 <small>Back-end</small> <br />
                                - MySQL Ver 14.14 Distrib 5.7.23, for Linux (x86_64) <br />
                            </p>
                            <a href="" class="restapp_link">read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of col-md-4-->
        </div>
        <!--End of row-->
    </div>
    <!--End of container-->
</section>


<section id="frontend">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_header text-center">
                    <h2>Front-End</h2>
                    <p>
                        The Front-end displays a list of users and a detail page for each of them. It is possible to make CRUD actions on them: create, update, delete. <br /><br />
                        A beautiful user interface would be appreciated&#9787;. <br /> <br />
                        The front-end must be developed on Codeigniter MVC Framework. It uses a REST Client (Curl, Guzzle…) to call the back-end API. <br /> <br />
                        In this Front / Back model, there is no model on the front-end side, Controller call the back-end directly.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="backend">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_header text-center">
                    <h2>Back-End</h2>
                    <p>
                        The back-end manages the database and shows REST API. API use an ORM or not to interact with the database. In Rise Up, we use NotORM. <br /><br />
                        You will have to create a User Resource to GET / POST / PUT / DELETE a user on the associated table. <br /><br />
                        On each action of the user, the front-end will communicate with the back-end in a Restful way.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="technologies">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section_header text-center">
                    <h2>Must-use technologies</h2>
                    <p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">PHP 7 <br/> <small> PHP 7.2.7-0ubuntu0.18.04.2 (cli)</small> </li>
                            <li class="list-group-item">CodeIgniter (Front-end) <br /> <small> CodeIgniter 3.1.9</small></li>
                            <li class="list-group-item">Slim Framework (Back-end) <br /> <small> Slim Framework 3.10.0</small></li>
                            <li class="list-group-item">Database of your choice <br /> <small> MySQL Ver 14.14 Distrib 5.7.23, for Linux (x86_64) </small></li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
