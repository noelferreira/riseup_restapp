<!--    Google Fonts-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

<!--Fontawesom-->
<link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css')?>">

<!--Animated CSS-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/animate.min.css')?>">

<!-- Bootstrap -->
<link href="<?php echo base_url('css/bootstrap.min.css')?>" rel="stylesheet">

<!--Main Stylesheet-->
<link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">
<!--Responsive Framework-->
<link href="<?php echo base_url('css/responsive.css')?>" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
