<div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="section_header" style="padding: 80px 0;">
                    
                    <h2 class="text-center">User details</h2>

                </div>

            </div>

        </div>

        <div class="col-md-8 well">

            
            <div class="row">

                <div class="col-md-12">

                    <h3>First name</h3>
                    
                    <p><?=$user->first_name?></p>
                    
                </div>

            </div>

            <hr />
            
            <div class="row">

                <div class="col-md-12">

                    <h3>Last name</h3>

                    <p><?=$user->last_name?></p>

                </div>

            </div>

            <hr />

            <div class="row">

                <div class="col-md-12">

                    <h3>Email</h3>

                    <p><?=$user->email?></p>

                </div>

            </div>

            <hr />

            <div class="row">
                
                <div class="col-md-12">

                    <h3>Notes</h3>

                    <p><?=$user->notes?></p>

                </div>

            </div>

            <hr />
            
            <div class="row">
                
                <div class="col-md-12">
                    
                    <h3>Active</h3>

                    <P><?=$user->active? 'Yes' : 'No'?></p>
                </div>
                
            </div>

            <hr />

        </div>
        
    <div class="col-md-4">

            <div class="row">
                
                <div class="col-md-12">

                    <a href="<?php echo site_url('Users/edit/'.$user->id)?>" role="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-pencil"></span> Update</a>

                </div>

            </div>

            <br />
            
            <div class="row">
                
                <div class="col-md-12">

                    <button data-tiele="Delete user" data-toggle="modal" data-target="#delete" type="button" class="btn btn-danger btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-trash"></span> Delete user</button>

                </div>
                
            </div>

            <hr />

            <a href="<?php echo site_url('Users/list_users')?>" role="button" class="btn btn-default btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Back</a>

    </div>

    
</div>

<!-- Modal Delete user: Start -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this user?</div>

            </div>
            <div class="modal-footer ">
                <a href="<?php echo site_url('Users/delete/'.$user->id)?>" role="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Modal Delete user: Start -->
